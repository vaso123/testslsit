<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class Admin extends AbstractController
{

    /**
     * @Route("/admin/", name="app_admin")
     */
    public function index() {
        return $this->render('admin/main.html.twig', ['title' => 'SlsIt főoldal']);
    }

}