<?php

namespace App\Controller\Admin;

use App\Security\PageAccessAuthorization;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class Page extends AbstractController
{

    /**
     * @var PageAccessAuthorizationI
     */
    private $pageAccessAutorization;

    public function __construct(PageAccessAuthorization $pageAccessAuthorization)
    {
        $this->pageAccessAutorization = $pageAccessAuthorization;
    }

    /**
     * @Route("/admin/pages/admin/", name="app.admin.page.admin")
     */
    public function admin()
    {
        if (!$this->checkRole(['ROLE_ADMIN'])) {
            return $this->redirectToRoute('app.error.forbidden');
        }

        return $this->render('admin/pages/admin.html.twig', ['title' => 'Oldal tartalom']);
    }

    /**
     * @Route("/admin/pages/editor/", name="app.admin.page.editor")
     */
    public function editor()
    {
        if (!$this->checkRole(['ROLE_EDITOR'])) {
            return $this->redirectToRoute('app.error.forbidden');
        }

        return $this->render('admin/pages/editor.html.twig', ['title' => 'Oldal tartalom']);
    }

    /**
     * @Route("/admin/pages/user/", name="app.admin.page.user")
     */
    public function user()
    {
        if (!$this->checkRole(['ROLE_USER'])) {
            return $this->redirectToRoute('app.error.forbidden');
        }

        return $this->render('admin/pages/user.html.twig', ['title' => 'Oldal tartalom']);
    }

    private function checkRole($roles)
    {
        /* @var UserInterface $user */
        $user = $this->getUser();
        if (empty($user)) {
            return false;
        }

        foreach ($roles as $role) {
            if ($this->pageAccessAutorization->canAccessPage($role)) {
                return true;
            }
        }

        return false;
    }
}
