<?php

namespace App\Controller;

use App\Library\Captcha\Captcha;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class Login extends AbstractController
{
    /**
     * @Route("/", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @param Captcha $captcha
     * @return Response
     */
    public function index(
        AuthenticationUtils $authenticationUtils,
        Captcha $captcha
    ): Response {
        /** @var UserInterface $user */
        $user = $this->getUser();
        if ($this->isGranted('IS_AUTHENTICATED_FULLY') && $user && $captcha->hasCapchaPassed()) {
            $this->redirectToRoute('app_admin');
        }
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUserName = $authenticationUtils->getLastUsername();
        $parameters = [
            'lastUsername' => $lastUserName,
            'error'        => $error,
            'showCaptcha'  => $captcha->shouldShow(),
        ];
        return $this->render('login.html.twig', $parameters);
    }

    /**
     * @Route("/kijelentkezes/", name="app_logout")
     */
    public function logout()
    {
        return $this->redirectToRoute('app_login');
    }
}
