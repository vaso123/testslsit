<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HtmlError extends AbstractController
{

    /**
     * @Route("/jogosultsagi_hiba/", name="app.error.forbidden")
     */
    public function forbidden() {
        return $this->render('error/403forbidden.html.twig');
    }
}
