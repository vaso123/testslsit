<?php

namespace App\EventListener;

use App\Entity\User;
use DateTime;
use Doctrine\ORM\EntityManager;
use Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Event\AuthenticationSuccessEvent;

class LoginListener
{

    /**
     * @var Session
     */
    private $session;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * @var EntityManager
     */
    private $entityManager;


    public function __construct(
        SessionInterface $session,
        ParameterBagInterface $parameterBag,
        EntityManager $entityManager
    ) {
        $this->session = $session;
        $this->parameterBag = $parameterBag;
        $this->entityManager = $entityManager;
    }

    public function onSecurityAuthenticationFailure()
    {
        $loginAttemptsKey = $this->parameterBag->get('login.attempts.key');
        $loginAttempts = $this->session->get($loginAttemptsKey);
        $this->session->set($loginAttemptsKey, ++$loginAttempts);
    }

    public function onSecurityAuthenticationSuccess(AuthenticationSuccessEvent $event)
    {
        $token = $event->getAuthenticationToken();
        $user = $token->getUser();
        if (!$user instanceof User) {
            return;
        }
        $this->updateLastLogin($user);
    }


    private function updateLastLogin(User $user)
    {
        try {
            $user->setLastLogin(new DateTime());
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        } catch (Exception $exception) {
            die($exception->getMessage());
        }
    }
}
