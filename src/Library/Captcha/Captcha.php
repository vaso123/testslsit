<?php

namespace App\Library\Captcha;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Captcha
{

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
        RequestStack $requestStack,
        ParameterBagInterface $parameterBag,
        SessionInterface $session
    ) {
        $this->requestStack = $requestStack;
        $this->parameterBag = $parameterBag;
        $this->session = $session;
    }

    public function captchaSuccess($recaptchaResponse)
    {
        if (empty($recaptchaResponse)) {
            return false;
        }
        $request = $this->requestStack->getCurrentRequest();
        $remoteIp = $request->server->get('REMOTE_ADDR');
        $secretKey = $this->parameterBag->get('recaptcha.secret.key');
        $captchaVerifyUrl = $this->parameterBag->get('recaptcha.verify.url');
        $url = $captchaVerifyUrl . '?' . http_build_query([
                'secret'   => $secretKey,
                'response' => $recaptchaResponse,
                'remoteip' => $remoteIp,

            ]);
        $response = file_get_contents($url);
        $responseKeys = json_decode($response, true);
        if (!$responseKeys["success"]) {
            return false;
        }

        return true;
    }

    public function hasCapchaPassed()
    {
        if (!$this->shouldShow()) {
            return true;
        }
        $request = $this->requestStack->getCurrentRequest();
        $captchaResponse = $request->request->get('g-recaptcha-response');
        return $this->captchaSuccess($captchaResponse);
    }

    public function shouldShow()
    {
        $loginAttemptsKey = $this->parameterBag->get('login.attempts.key');
        if ($this->session->get($loginAttemptsKey) >= $this->parameterBag->get('login.attempts.treshold')) {
            return true;
        }

        return false;
    }
}
