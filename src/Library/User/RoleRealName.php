<?php

namespace App\Library\User;

class RoleRealName
{

    const ROLE_NAMES = [
        'ROLE_ADMIN' => 'Adminisztrátor',
        'ROLE_EDITOR' => 'Tartalomszerkesztő',
        'ROLE_USER' => 'Bejelentkezett felhasználó',
    ];

    public function allTranslatedRoleNames($roles) {
        $translated = [];
        foreach ($roles as $key => $roleName) {
            $translated[$key] = $this->nameByKey($roleName);
        }
        return $translated;
    }

    public function nameByKey($key) {
        if (!array_key_exists($key, self::ROLE_NAMES)) {
            die('No array key exists in roles: ' . $key);
        }
        return self::ROLE_NAMES[$key];
    }
}
