<?php

namespace App\Library\User;

use App\Entity\UserSession;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserLogin
{
    /**
     * @var RoleRealName
     */
    private $roleRealName;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        SessionInterface $session,
        RoleRealName $roleRealName,
        ParameterBagInterface $parameterBag,
        EntityManagerInterface $entityManager

    ) {
        $this->session = $session;
        $this->roleRealName = $roleRealName;
        $this->parameterBag = $parameterBag;
        $this->entityManager = $entityManager;
    }

    public function saveRolesSession(UserInterface $user)
    {
        $userRoles = $this->userRolesKeys($user);
        $roleNames = $this->roleRealName->allTranslatedRoleNames($userRoles);
        $this->session->set('roleNames', $roleNames);
    }

    public function saveSession(UserInterface $user, SessionInterface $session) {
        $userSession = $this->userSession($user, $session);
        $this->entityManager->persist($userSession);
        $this->entityManager->flush();
    }

    private function userSession(UserInterface $user, SessionInterface $session) {
        $userSesson = new UserSession();
        $userSesson->setData(serialize($session));
        $userSesson->setCreationDate(new DateTime());
        $userSesson->setUser($user);
        return $userSesson;
    }

    private function userRolesKeys(UserInterface $user) {
        $allRoles = $this->parameterBag->get('security.role_hierarchy.roles');
        $userRoles = $user->getRoles();
        $roles = [];
        foreach ($userRoles as $userRole) {
            if (array_key_exists($userRole, $allRoles)) {
                $this->addRoles($roles, $allRoles[$userRole]);
            }
        }
        return array_unique($roles);
    }

    private function addRoles(&$roles, $roleEntries) {
        $values = array_values($roleEntries);
        foreach ($values as $value) {
            $roles[] = $value;
        }
    }

    public function resetLoginAttempts()
    {
        $this->session->set($this->parameterBag->get('login.attempts.key'), 0);
    }
}