<?php

namespace App\Entity;

use App\Repository\UserRoleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserRole", mappedBy="user")
     */
    private $userRoles;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastLogin;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserSession", mappedBy="user")
     */
    private $session;

    public function __construct(UserRoleRepository $userRoleRepository)
    {
        $this->userRoles = new ArrayCollection();
        $this->session = new ArrayCollection();
    }

    //UserInterface methods
    public function eraseCredentials()
    {
        //Empty method. This is ok.
    }

    public function getUserName()
    {
        return $this->getName();
    }

    public function getRoles()
    {

        $userRoles = [];
        if ($this->getId()) {
            $roles = $this->getUserRoles()->getValues();
            foreach ($roles as $role) {
                /* @var UserRole $userRole */
                $userRole = $role;
                $userRoles[] = $userRole->getName();
            }
        }
        return $userRoles;
    }

    public function getSalt()
    {
        return null;
    }

    //Entity methods

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection|UserRole[]
     */
    public function getUserRoles(): Collection
    {
        return $this->userRoles;
    }

    public function addUserRole(UserRole $userRole): self
    {
        if (!$this->userRoles->contains($userRole)) {
            $this->userRoles[] = $userRole;
            $userRole->setUser($this);
        }

        return $this;
    }

    public function removeUserRole(UserRole $userRole): self
    {
        if ($this->userRoles->contains($userRole)) {
            $this->userRoles->removeElement($userRole);
            // set the owning side to null (unless already changed)
            if ($userRole->getUser() === $this) {
                $userRole->setUser(null);
            }
        }

        return $this;
    }

    public function getLastLogin(): ?\DateTime
    {
        return $this->lastLogin;
    }

    public function setLastLogin(\DateTime $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * @return Collection|UserSession[]
     */
    public function getSession(): Collection
    {
        return $this->session;
    }

    public function addSession(UserSession $session): self
    {
        if (!$this->session->contains($session)) {
            $this->session[] = $session;
            $session->setUser($this);
        }

        return $this;
    }

    public function removeSession(UserSession $session): self
    {
        if ($this->session->contains($session)) {
            $this->session->removeElement($session);
            // set the owning side to null (unless already changed)
            if ($session->getUser() === $this) {
                $session->setUser(null);
            }
        }

        return $this;
    }
}
