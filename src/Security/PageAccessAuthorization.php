<?php

namespace App\Security;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class PageAccessAuthorization
{

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }


    public function canAccessPage($role) {
        return $this->authorizationChecker->isGranted($role);
    }

}