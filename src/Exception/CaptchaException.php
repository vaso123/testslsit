<?php

namespace App\Exception;

use Symfony\Component\Security\Core\Exception\AuthenticationException;

class CaptchaException extends AuthenticationException
{
    public function getMessageKey()
    {
        return 'Are you a robot?';
    }
}
